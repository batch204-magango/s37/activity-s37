const User = require("../models/User");
//s41
const Course = require("../models/Course");
//----------------------------
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExists = (reqBody)=> {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}







//Controller for User Registration

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,

		//bcrypt.hashSync(<dataTobeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})



	return newUser.save().then((user, error)=> {

		if(error) {
			return false
		} else {
			return true
		}
	})
}





//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			//bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>)

			if(isPasswordCorrect) {
				console.log(result)
				return {
					access:auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//activity
//Create a getProfile controller method for retrieving the details of the user:

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.id).then(result => {
		result.password = ""
		return result
	}).catch(error => "id not found")
}


//Enroll user to a class
module.exports.enroll = async (data)=> {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error)=> {

			if(error) {
				return false
			} else {
				return true
			}

		});

	});

	let isCourseUpdated = await Course.findById(data.courseId). then(course => {
		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error)=> {
			if(error) {
				return false
			} else {
				return true
			}
		});
	});


	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}



}

//Enroll user to a class
 // module.exports.enroll = async (data) => {
 // 	if(data.isAdmin)
 // }