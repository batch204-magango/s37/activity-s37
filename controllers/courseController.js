const Course = require("../models/Course");
const User = require("../models/User");
//const courseController = require("../controllers/courseController");


//Create a New Course
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	return newCourse.save().then( (course, error) => {

		if(error) {

			return false

		} else {

			return true
		}

	});
}

//Retrieve all courses
module.exports.getAllCourses =()=> {
	return Course.find({}).then(result => {
		return result;
	})
}


//controller for retrieving all active course
module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then (result => {
		return result;
	})
}

//Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


//Updating Course
// module.exports.updateCourse = (reqParams, reqBody, data) => {


// 	if(data.isAdmin === true) {
// 		let updatedCourse = {
// 			name:reqBody.name,
// 			description: reqBody.description,
// 			price:reqBody.price
// 		};
// 		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
// 			(course, error)=> {
// 				if(error) {
// 					return false
// 				} else {
// 					return true
// 				}
// 			})
// 	} else {
// 		return false
// 	}



// }



//------------------
//update course 10-12-22

module.exports.updateCourse = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // Specify the fields/properties of the document to be updated
            let updatedCourse = {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            };

            // Syntax:
                        // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

                // Course not updated
                if(error) {

                    return false

                // Course updated successfully    
                } else {

                    return true
                }
            })

        } else {

            return false
        }



    })


}

	//Archiving course


module.exports.archiveCourse = (reqParams, reqBody, data) => {

	if(data.isAdmin === true){
		let archivedCourse = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false
			}
			else{
				return true
			}
		})
	}
	else{
		return false
	}
}
